#!/usr/bin/env python
# coding: utf-8

# # Chess Queens EDA using Plotly
# #By- Aarush Kumar
# #Dated: July 23,2021

# In[1]:


from IPython.display import Image
Image(url='https://c4.wallpaperflare.com/wallpaper/765/279/741/chess-shadow-pawns-500px-wallpaper-preview.jpg')


# ## 1. Importing Libraries and Reading the dataset

# In[2]:


import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from mpl_toolkits.mplot3d import Axes3D
from plotly.offline import init_notebook_mode, iplot
import plotly.figure_factory as ff
import cufflinks
cufflinks.go_offline()
cufflinks.set_config_file(world_readable=True, theme='pearl')
import plotly.graph_objs as go
import plotly
from plotly import tools
import plotly.express as px
from scipy.stats import boxcox
init_notebook_mode(connected=True)


# In[3]:


chess = pd.read_csv('/home/aarush100616/Downloads/Projects/Chess Queens/top_women_chess_players_aug_2020.csv')
chess


# ## 2. Inspecting the data

# In[4]:


chess.shape


# In[5]:


chess.size


# In[7]:


chess.info()


# In[8]:


chess.isnull().sum()


# In[9]:


chess.dtypes


# In[10]:


chess.describe()


# ### 2.1. Finding columns with high null values

# In[11]:


null_perc = chess.isnull().sum()/len(chess)*100
null_perc.sort_values(ascending = False).head(10)


# In[12]:


chess.Title.value_counts(dropna=False)


# In[13]:


df = chess.Title.fillna('Title not known')
df.value_counts()


# In[14]:


chess.Inactive_flag.value_counts(dropna=False)


# In[15]:


chess.Inactive_flag = chess.Inactive_flag.fillna('Active')
chess.Inactive_flag = chess.Inactive_flag.astype(str).replace('wi','Inactive')
chess.Inactive_flag.value_counts(dropna=False)


# In[16]:


chess['Current_Year'] = 2020
chess['Age'] = chess['Current_Year'] - chess['Year_of_birth']
chess.head()


# In[17]:


fig = px.box(chess, y="Age",title='Distribution of Age' )
fig.show()


# In[18]:


fig = px.box(chess, y="Standard_Rating",title='Distribution of Standard Rating' )
fig.show()


# In[19]:


fig = px.box(chess, y="Rapid_rating",title='Distribution of Rapid Rating' )
fig.show()


# In[20]:


fig = px.box(chess, y="Blitz_rating",title='Distribution of Blitz Rating' )
fig.show()


# ## 3. Analysis

# In[21]:


temp = chess["Federation"].value_counts().head(30)
temp.iplot(kind='bar', xTitle = 'Country', yTitle = "Count", title = 'Top 30 Countries by Number of Top Ranked Female Chess Players', color = '#4CB391')


# In[22]:


im = chess["Inactive_flag"].value_counts()
df = pd.DataFrame({'labels': im.index,'values': im.values})
df.iplot(kind='pie',labels='labels',values='values', title='Status of Top Ranked Female Chess Players', hole = 0.5, colors=['#FF414D','#9B116F'])


# In[23]:


temp = chess["Title"].value_counts().head(30)
temp.iplot(kind='bar', xTitle = 'Country', yTitle = "Count", title = 'Count of Title of Top Ranked Female Chess Players', color = '#FF8E15')


# In[24]:


fig = px.histogram(chess, x = 'Year_of_birth', color="Inactive_flag", title = 'Year of Birth Distribution with active status of Top Female Chess Players ')
fig.show()


# In[25]:


fig = px.histogram(chess, x = 'Age', color="Inactive_flag", title = 'Age Distribution with active status of Top Female Chess Players ')
fig.show()


# In[26]:


gm = chess[chess['Title']=='GM']
temp = gm.groupby('Federation')['Name'].count().sort_values(ascending=False).head(10)
temp.iplot(kind='bar', xTitle = 'Country', yTitle = "Count", title = 'Countries with most number of female Grand Masters', color = '#FD7055')


# In[27]:


temp = chess.sort_values(by = 'Standard_Rating', ascending = False).head(10)
fig = px.funnel(temp, y = 'Name', x = 'Standard_Rating', title = 'Top 10 Female Chess Players based on Standard Rating', color = 'Inactive_flag')
fig.show()


# In[28]:


temp = chess.sort_values(by = 'Rapid_rating', ascending = False).head(10)
fig = px.funnel(temp, y = 'Name', x = 'Rapid_rating', title = 'Top 10 Female Chess Players based on Rapid Rating', color = 'Inactive_flag')
fig.show()


# In[29]:


temp = chess.sort_values(by = 'Blitz_rating', ascending = False).head(10)
fig = px.funnel(temp, y = 'Name', x = 'Blitz_rating', title = 'Top 10 Female Chess Players based on Blitz Rating', color = 'Inactive_flag' )
fig.show()


# In[30]:


fig = px.density_heatmap(chess, x="Standard_Rating", y="Rapid_rating", marginal_x="histogram", marginal_y="histogram")
fig.show()


# In[31]:


fig = px.density_contour(chess, x="Standard_Rating", y="Blitz_rating", color="Inactive_flag", marginal_x="histogram", marginal_y="histogram")
fig.show()


# In[32]:


fig = px.density_heatmap(chess, x="Rapid_rating", y="Blitz_rating", color_continuous_scale="tropic")
fig.show()


# ## 4. Map Visuals

# In[33]:


df_fed = pd.DataFrame(chess.groupby('Federation').size()).reset_index()
df_fed.rename(columns = {0:'Number of Chess Players'}, inplace=True)
fig = px.choropleth(df_fed, locations="Federation",
                    color="Number of Chess Players",
                    hover_name="Federation", 
                    color_continuous_scale="tealgrn",
                    title = 'Top Ranked Female Chess Players by Country')
fig.show()


# In[34]:


gm_fed = pd.DataFrame(gm.groupby('Federation').size()).reset_index()
gm_fed.rename(columns = {0:'Number of Grandmasters'}, inplace=True)


# In[35]:


fig = px.choropleth(gm_fed, locations="Federation",
                    color="Number of Grandmasters", 
                    hover_name="Federation", 
                    color_continuous_scale='portland',
                    title = 'Female Grandmasters by Country')
fig.show()


# ## 5. Conclusions

# * Judit Polgar is the top female chess player topping all three of the ratings, i.e. Standard Rating, Rapid Rating and Blitz Rating.
# * Russia is the leader when it comes to top female chess players although China tops the list with the highest number of Grand Masters with a count of 7.
# * When it comes to female Grand Masters, Georgia ranks 3rd with 5 Grand Masters.
# * There are only 37 female Grand Masters in the world.
# * India has 2 female Grand Masters.
